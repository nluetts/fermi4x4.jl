module Fermi4x4

using Clustering: kmeans
using Distributions
using Distributions: FillArrayOfUnivariateDistribution, mean_logp
using LinearAlgebra
using MonteCarloMeasurements: Particles
using Printf
using Optim: optimize, minimizer
using Random
using StaticArrays

export
    Band,
    bands,
    couplings_4x4,
    couplings_3x3,
    ucouplings_3x3,
    ucouplings_4x4,
    couple!,
    uncouple!,
    group_uncertain,
    select_most_common,
    select_valid,
    verify_result,
    solve

##############################################################################
##  Types and Consts   #######################################################
##############################################################################

const EPS = eps(Float64)
const F64MAX = prevfloat(typemax(Float64))
const FermiFloat = Union{Float64,Particles{Float64,N} where N}

RETRIES::Int64 = 30
NUM_PARTICLES = 10000
CONV_THRES::Float64 = 1e-3
VERIFY_VALS_THRES::Float64 = 0.1
VERIFY_VECS_THRES::Float64 = 0.001

mutable struct Band{T<:FermiFloat}
    nu::T
    int::T
end

mutable struct Constraint{T<:FermiFloat}
    is_set::Bool
    value::T
    other::Union{Nothing,Tuple{Int64,Int64}}
end

struct Couplings{T<:FermiFloat,S}
    couplings::SMatrix{S,S,Constraint{T}}
end


##############################################################################
##  public API  ##############################################################
##############################################################################

_warn_verify() = @warn "Resulting D matrix exceeded thresholds when checking for conformity with inputs; run with show_verify=true for details."

"""
    function solve(
        bands::SVector{S,Band{Float64}},
        couplings::Couplings{Float64,S},
        args...;
        retries=nothing,
        verify=true,
        show_verify=false,
        seed::Union{Nothing,Int64}=nothing,
        kw...
    )::Matrix{Float64} where {S}

Returns the matrix D that solves the SxS Fermi problem.

`verify = true` (true by default) runs the `verify_result()` function on the
resulting D matrix. It is verified that the inputs (wavenumbers and intensities)
can be recovered from the eigenvalues and eigenvectors of the resulting D
matrix.

Additional keyword arguments are passed to `Optim.optimize`.
"""
function solve(
    bands::SVector{S,Band{Float64}},
    couplings::Couplings{Float64,S},
    args...;
    retries=nothing,
    verify=true,
    show_verify=false,
    seed::Union{Nothing,Int64}=nothing,
    kw...
)::Matrix{Float64} where {S}
    !isnothing(seed) && seed!(seed)
    D_elements = _solve(unpack(bands)..., unpack(couplings)..., args...; retries=retries, kw...)
    D = zeros(S, S)
    for i in 1:S, j in i:S
        D[i, j] = D_elements[index_D(Val(S), i, j)]
        if i != j
            D[j, i] = D_elements[index_D(Val(S), i, j)]
        end
    end
    if verify
        _, _, flagged = verify_result(bands, D, VERIFY_VALS_THRES, VERIFY_VECS_THRES; silent=!show_verify)
        (!show_verify && flagged) && _warn_verify()
    end
    D
end

"""
    function solve(
        bands::SVector{3,Band{Particles{Float64,N}}},
        couplings::Couplings{Particles{Float64,N},3},
        args...;
        retries=nothing,
        verify=true,
        show_verify=false,
        warn_retries=true,
        seed::Union{Nothing,Int64}=nothing,
        kw...
    )::Matrix{Particles{Float64,N}} where {N}

Returns the matrix D that solves the 3x3 Fermi problem.

`verify = true` (true by default) runs the `verify_result()` function on the
resulting D matrix. It is verified that the inputs (wavenumbers and intensities)
can be recovered from the eigenvalues and eigenvectors of the resulting D
matrix.

If `warn_retries == true`, emit a warning if optimization fails
to find a solutions that fulfills the constraints.

Additional keyword arguments are passed to `Optim.optimize`.
"""
function solve(
    bands::SVector{3,Band{Particles{Float64,N}}},
    couplings::Couplings{Particles{Float64,N},3},
    args...;
    retries=nothing,
    verify=true,
    show_verify=false,
    warn_retries=true,
    seed::Union{Nothing,Int64}=nothing,
    kw...
)::Matrix{Particles{Float64,N}} where {N}
    !isnothing(seed) && seed!(seed)
    f1, f2, f3, b1, b2, b3 = unpack(bands)
    w12, w13, w23 = unpack(couplings)
    D = Particles(zeros(N, 3, 3))
    for n in 1:N
        D_elements = _solve(
            f1.particles[n],
            f2.particles[n],
            f3.particles[n],
            b1.particles[n],
            b2.particles[n],
            b3.particles[n],
            (w12[1].particles[n], w12[2]),
            (w13[1].particles[n], w13[2]),
            (w23[1].particles[n], w23[2]),
            args...;
            retries=retries,
            silent=!warn_retries,
            kw...
        )
        for i in 1:3, j in i:3
            D[i, j].particles[n] = D_elements[index_D(Val(3), i, j)]
            if i != j
                D[j, i].particles[n] = D_elements[index_D(Val(3), i, j)]
            end
        end
    end
    if verify
        _, idx_flagged = verify_result(bands, D, VERIFY_VALS_THRES, VERIFY_VECS_THRES; silent=!show_verify, verbose=show_verify)
        (!show_verify && length(idx_flagged) > 0) && _warn_verify()
    end
    D
end

"""
    function solve(
        bands::SVector{4,Band{Particles{Float64,N}}},
        couplings::Couplings{Particles{Float64,N},4},
        args...;
        retries=nothing,
        verify=true,
        show_verify=false,
        warn_retries=true,
        seed::Union{Nothing,Int64}=nothing,
        kw...
    )::Matrix{Particles{Float64,N}} where {N}

Returns the matrix D that solves the 4x4 Fermi problem.

`verify = true` (true by default) runs the `verify_result()` function on the
resulting D matrix. It is verified that the inputs (wavenumbers and intensities)
can be recovered from the eigenvalues and eigenvectors of the resulting D
matrix.

If `warn_retries == true`, emit a warning if optimization fails
to find a solutions that fulfills the constraints.

Additional keyword arguments are passed to `Optim.optimize`.
"""
function solve(
    bands::SVector{4,Band{Particles{Float64,N}}},
    couplings::Couplings{Particles{Float64,N},4},
    args...;
    retries=nothing,
    verify=true,
    show_verify=false,
    warn_retries=true,
    seed::Union{Nothing,Int64}=nothing,
    kw...
)::Matrix{Particles{Float64,N}} where {N}
    !isnothing(seed) && seed!(seed)
    f1, f2, f3, f4, b1, b2, b3, b4 = unpack(bands)
    w12, w13, w14, w23, w24, w34 = unpack(couplings)
    D = Particles(zeros(N, 4, 4))
    for n in 1:N
        D_elements = _solve(
            f1.particles[n],
            f2.particles[n],
            f3.particles[n],
            f4.particles[n],
            b1.particles[n],
            b2.particles[n],
            b3.particles[n],
            b4.particles[n],
            (w12[1].particles[n], w12[2]),
            (w13[1].particles[n], w13[2]),
            (w14[1].particles[n], w14[2]),
            (w23[1].particles[n], w23[2]),
            (w24[1].particles[n], w24[2]),
            (w34[1].particles[n], w34[2]),
            args...;
            retries=retries,
            silent=!warn_retries,
            kw...
        )
        for i in 1:4, j in i:4
            D[i, j].particles[n] = D_elements[index_D(Val(4), i, j)]
            if i != j
                D[j, i].particles[n] = D_elements[index_D(Val(4), i, j)]
            end
        end
    end
    if verify
        _, idx_flagged = verify_result(bands, D, VERIFY_VALS_THRES, VERIFY_VECS_THRES; silent=!show_verify, verbose=show_verify)
        (!show_verify && length(idx_flagged) > 0) && _warn_verify()
    end
    D
end


"""
    function couple!(couplings::Couplings{T,S}, i, j, value) where {T<:FermiFloat,S}

Constrain the coupling element with index (i, j) in the D matrix to a specific value.
"""
function couple!(couplings::Couplings{T,S}, i, j, value) where {T<:FermiFloat,S}
    value = T(value)
    @assert S >= i > 0 "State indices for $(S)×$(S) problem must be > 0 and < $(S + 1), got i = $i."
    @assert S >= j > 0 "State indices for $(S)×$(S) problem must be > 0 and < $(S + 1), got j = $j."
    @assert i != j "You cannot couple a state with itself."
    set!(couplings.couplings[i, j], value)
    set!(couplings.couplings[j, i], value)
    nothing
end

"""
    function couple!(couplings::Couplings{T,S}, i, j, k, l, ratio) where {T<:FermiFloat,S}

Constrain a ratio between the D-matrix elements (i, j) and (k, l).
"""
function couple!(couplings::Couplings{T,S}, i, j, k, l, ratio) where {T<:FermiFloat,S}
    ratio = T(ratio)
    @assert S >= i > 0 "State indices for $(S)×$(S) problem must be > 0 and < $(S + 1), got i = $i."
    @assert S >= j > 0 "State indices for $(S)×$(S) problem must be > 0 and < $(S + 1), got j = $j."
    @assert S >= k > 0 "State indices for $(S)×$(S) problem must be > 0 and < $(S + 1), got k = $k."
    @assert S >= l > 0 "State indices for $(S)×$(S) problem must be > 0 and < $(S + 1), got l = $l."
    @assert i != j "Setting diagonal elements (i == j) is not supported."
    @assert k != l "Linear dependency on diagonal element (k == l) is not supported."
    @assert !(k == i && l == j) "(i, j) == (k, l) is not allowed."
    set!(couplings.couplings[i, j], ratio, (k, l))
    nothing
end

"""
    function uncouple!(c::Couplings{T,S}, i, j) where {T<:FermiFloat,S}

Constrain the coupling element with index (i, j) in the D matrix to zero.
"""
uncouple!(c::Couplings{T,S}, i, j) where {T<:FermiFloat,S} = couple!(c, i, j, zero(T))

"""
    function group_uncertain(bds::SVector{S,Band{Particles{Float64, N}}}, D::Matrix{Particles{Float64,N}}, nclasses=2) where {N, S}

Post-process an uncertain D-matrix by grouping the results.

Groups are selected by Kmeans clustering. The number of groups can be
controlled by the argument `nclasses` (defaults to 2, i.e. one main group
and the "rest").

Returns list of tuples where the first tuple element are the bands of a group and
the second element is the D matrix of the same group.
"""
function group_uncertain(bds::SVector{S,Band{Particles{Float64,N}}}, D::Matrix{Particles{Float64,N}}, nclasses=2) where {N,S}
    preprocessed = preprocess_for_clustering(D)
    km = kmeans(preprocessed, nclasses)
    classes = unique(km.assignments)
    groups = []
    for class in classes
        mask = km.assignments .== class
        cbands = map(b -> Band(Particles(b.nu.particles[mask]), Particles(b.int.particles[mask])), bds)
        cD = map(x -> Particles(x.particles[mask]), D)
        push!(groups, (cbands, cD))
    end
    #                  ↓ picks D ↓
    sortfun(x) = x |> last |> first |> num_particles
    sort!(groups; by=sortfun, rev=true)
    groups
end

"""
    function select_most_common(
            bds::SVector{S, Band{Particles{Float64, N}}},
            D::Matrix{Particles{Float64,N}}, nclasses=2;
            subsamples=nothing
    ) where {N, S}

Post-process an uncertain D-matrix by selecting the most common group of
results.

Groups are selected by Kmeans clustering. The number of groups can be
controlled by the argument `nclasses` (defaults to 2, i.e. one main group
and the "rest").

Since further processing the result with other uncertain numbers requires
the same number of samples, the sample size can be reduced to a common
number with the keyword argument `subsamples`.
"""
function select_most_common(
    bds::SVector{S,Band{Particles{Float64,N}}},
    D::Matrix{Particles{Float64,N}}, nclasses=2;
    subsamples=nothing
) where {N,S}
    bds, D = group_uncertain(bds, D, nclasses) |> first
    if !isnothing(subsamples)
        samples = length(D[1, 1].particles)
        @assert subsamples <= samples "Not enough samples for subsampling ($samples present vs. $subsamples wanted)"
        selection = 1:subsamples
        D = map(x -> Particles(x.particles[selection]), D)
        bds = map(x -> Band(x.nu.particles[selection], x.int.particles[selection]), bds)
    end
    bds, D
end

"""
        function verify_result(bands, D_matrix, threshold_vals=0.01, threshold_vecs=0.001; silent=false)

    Diagonalize the D-matrix and check that the resulting eigenvalues and eigenvectors are compatible
    with the input band positions and intensitites.

    The arguments `threshold_vals` and `threshold_vecs` define how large the deviation for values and
    vector components (derived from the intensities) are allowed to be before a warning is emitted.

    When this function runs automatically from within the `solve()` function, the thresholds are
    set to `Fermi4x4.VERIFY_VALS_THRES` and `Fermi4x4.VERIFY_VECS_THRES`.
    You can change these settings to tweak the detection threshold when `solve`ing.
"""
function verify_result(bds::SVector{S,Band{Float64}}, D::Matrix{Float64}, threshold_vals=0.01, threshold_vecs=0.001; silent=false, draw=nothing) where {S}
    deviation(x, y) = begin
        x, y = abs(x), abs(y)
        abs(x - y)
    end
    bds = sort(bds; by=x -> x.nu)
    wavenumbers = map(b -> b.nu, bds)
    norm = map(b -> b.int, bds) |> sum
    vec_from_input = map(b -> √(b.int / norm), bds)

    vals, vecs = eigen(D)

    index_matching_vec = 0
    max_deviation_vecs = 1e9
    max_deviation_vals = 0.0
    for (n, (vec_from_D, wn_from_D, wn_from_input)) in zip(eachrow(vecs), vals, wavenumbers) |> enumerate
        max_dev_vec = 0.0
        for (vi, ci) in zip(vec_from_D, vec_from_input)
            δ_component = deviation(vi, ci)
            # here we look for the maximum for each components
            max_dev_vec = δ_component > max_dev_vec ? δ_component : max_dev_vec
        end
        max_dev_val = deviation(wn_from_D, wn_from_input)
        max_deviation_vals = max_dev_val > max_deviation_vals ? max_dev_val : max_deviation_vals
        # here we look for the minimum for all vectors, because
        # the input vector will match only one of the vectors
        # computed via `eigen(D)`
        if max_dev_vec < max_deviation_vecs
            max_deviation_vecs = max_dev_vec
            index_matching_vec = n
        end
    end

    flagged = max_deviation_vecs > threshold_vecs || max_deviation_vals > threshold_vals
    if silent
        # return early if no output is wanted
        return (max_deviation_vals, max_deviation_vecs, flagged)
    end

    # generate output
    digits_vals = log10(threshold_vals) |> abs |> ceil |> Int
    digits_vecs = log10(threshold_vecs) |> abs |> ceil |> Int
    format_vals = Printf.Format("%.$(digits_vals)f (input) vs. %.$(digits_vals)f (output), deviation = %.$(digits_vals+1)f\n")
    format_vecs = Printf.Format("%.$(digits_vecs)f (input) vs. %.$(digits_vecs)f (output), deviation = %.$(digits_vecs+1)f\n")
    draw_str = isnothing(draw) ? "" : " (draw $draw)"
    if max_deviation_vals > threshold_vals
        @warn "Deviation of eigenvalues exceeds threshold ($threshold_vals)$draw_str:"
        for (in, out) in zip(wavenumbers, vals)
            dev = deviation(in, out)
            c = dev > threshold_vals ? (:red) : (:normal)
            printstyled(Printf.format(format_vals, in, out, dev); color=c)
        end
        println()
    end
    if max_deviation_vecs > threshold_vecs
        @warn "Deviation of eigenvector components exceeds threshold ($threshold_vecs)$draw_str:"
        for (in, out) in zip(vec_from_input, vecs[index_matching_vec, :])
            dev = deviation(in, out)
            c = dev > threshold_vecs ? (:red) : (:normal)
            printstyled(Printf.format(format_vecs, in, out, dev); color=c)
        end
        println("\nAssociated eigenvectors:$draw_str")
        display(vecs)
        println()
    end
    if max_deviation_vecs > threshold_vecs || max_deviation_vals > threshold_vals
        println("Associated D matrix$draw_str:")
        display(D)
        println()
    end
    (max_deviation_vals, max_deviation_vecs, flagged)
end

function verify_result(bds::SVector{S,Band{Particles{Float64,N}}}, D::Matrix{Particles{Float64,N}}, threshold_vals=0.01, threshold_vecs=0.001; silent=false, verbose=false) where {N,S}
    max_deviations = zeros(N, 2)
    idx_flagged = Int[]
    for n in 1:N
        Dn = map(x -> x.particles[n], D)
        bdsn = map(x -> Band(x.nu.particles[n], x.int.particles[n]), bds)
        max_deviations[n, 1], max_deviations[n, 2], flagged = verify_result(bdsn, Dn, threshold_vals, threshold_vecs; draw=n, silent=!verbose)
        if flagged
            push!(idx_flagged, n)
        end
    end
    if length(idx_flagged) > 0 && !silent
        percent = round(100length(idx_flagged) / N, digits=1)
        println("$(length(idx_flagged)) out of $N draws ($percent %) exceed the deviation thresholds (eigenvalues: $threshold_vals, eigenvectors: $threshold_vecs).")
    end
    max_deviations, idx_flagged
end

"""
    function select_valid(bds::SVector{S,Band{Particles{Float64,N}}}, D::Matrix{Particles{Float64,N}}, threshold_vals=0.01, threshold_vecs=0.001; silent=false) where {N,S}

Select samples for which `verify_result()` succeds.

For an explanation of the arguments, see `verify_results()`.
"""
function select_valid(bds::SVector{S,Band{Particles{Float64,N}}}, D::Matrix{Particles{Float64,N}}, threshold_vals=0.01, threshold_vecs=0.001; silent=false, verbose=false) where {N,S}
    _, mask = verify_result(bds, D, threshold_vals, threshold_vecs; silent=silent, verbose=verbose)
    mask = [n for n in 1:N if n ∉ mask] # invert mask to select valid draws
    D = map(x -> Particles(x.particles[mask]), D)
    bds = map(b -> Band(Particles(b.nu.particles[mask]), Particles(b.int.particles[mask])), bds)
    bds, D
end


##############################################################################
##  Constructors   ###########################################################
##############################################################################

function Band(nu, int)
    nu = Float64(nu)
    int = Float64(int)
    Band{Float64}(nu, int)
end

"""Bundle a set of bands 3 or 4 as input for the `solve` function."""
bands(bd1::Band{T}, bd2::Band{T}, bd3::Band{T}) where {T} = SVector{3,Band{T}}(bd1::Band{T}, bd2::Band{T}, bd3::Band{T})
bands(bd1::Band{T}, bd2::Band{T}, bd3::Band{T}, bd4::Band{T}) where {T} = SVector{4,Band{T}}(bd1::Band{T}, bd2::Band{T}, bd3::Band{T}, bd4::Band{T})

function Constraint{T}() where {T}
    Constraint{T}(false, zero(T), nothing)
end

function Constraint(val::T) where {T}
    Constraint{T}(true, val, nothing)
end


function Couplings(::Type{T}, n::S) where {S<:Integer,T<:FermiFloat}
    @assert n in 3:4 "Problem size must be 3 or 4, got $n."
    mat = SMatrix{n,n,Constraint{T}}(Constraint{T}() for _ in 1:(n^2))
    Couplings{T,n}(mat)
end


init_couplings(::Val{S}) where {S} = Couplings(Float64, S)
init_ucouplings(::Val{S}) where {S} = Couplings(Particles{Float64,NUM_PARTICLES}, S)

"""Initialize a set of coupling constraints for the 3x3 problem."""
couplings_3x3() = init_couplings(Val(3))
"""Initialize a set of coupling constraints for the 4x4 problem."""
couplings_4x4() = init_couplings(Val(4))

"""Initialize a set of coupling constraints for the 3x3 problem (uncertain inputs)."""
ucouplings_3x3() = init_ucouplings(Val(3))
"""Initialize a set of coupling constraints for the 4x4 problem (uncertain inputs)."""
ucouplings_4x4() = init_ucouplings(Val(4))


##############################################################################
##  Couplings and Constraint handling ########################################
##############################################################################

function set!(constraint::Constraint{T}, value, other=nothing) where {T}
    constraint.value = T(value)
    constraint.is_set = true
    constraint.other = other
    nothing
end

function Base.show(io::IO, cc::Constraint{T}) where {T}
    if cc.is_set && isnothing(cc.other)
        Base.show_unquoted(io, cc.value)
    elseif cc.is_set && !isnothing(cc.other)
        Base.show_unquoted(io, "$(cc.value) -> $(cc.other)")
    else
        print(io, "unset")
    end
end


function Base.display(couplings::Couplings{T,N}) where {T,N}
    display(Couplings{T})
    display(couplings.couplings)
end

##############################################################################
##  Calculation of D matrix  #################################################
##############################################################################

# D-matrix elements of 4x4 problem
D3(::Val{1}, ::Val{1}, f1, f2, f3, b1, b2, b3, ct, st) =
    (b1^2) * f1 + (b2^2) * f2 + (b3^2) * f3
D3(::Val{1}, ::Val{2}, f1, f2, f3, b1, b2, b3, ct, st) =
    -(-(b1^2) * √(1 / (b1^2 + b2^2)) - (b2^2) * √(1 / (b1^2 + b2^2))) * b3 * f3 * st + b1 * (-b2 * ct * √(1 / (b1^2 + b2^2)) - b1 * b3 * √(1 / (b1^2 + b2^2)) * st) * f1 + b2 * (b1 * ct * √(1 / (b1^2 + b2^2)) - b2 * b3 * √(1 / (b1^2 + b2^2)) * st) * f2
D3(::Val{1}, ::Val{3}, f1, f2, f3, b1, b2, b3, ct, st) =
    (-(b1^2) * √(1 / (b1^2 + b2^2)) - (b2^2) * √(1 / (b1^2 + b2^2))) * b3 * f3 * ct + b1 * (-b2 * √(1 / (b1^2 + b2^2)) * st + b1 * b3 * ct * √(1 / (b1^2 + b2^2))) * f1 + b2 * (b1 * √(1 / (b1^2 + b2^2)) * st + b2 * b3 * ct * √(1 / (b1^2 + b2^2))) * f2
D3(::Val{2}, ::Val{2}, f1, f2, f3, b1, b2, b3, ct, st) =
    ((-(b1^2) * √(1 / (b1^2 + b2^2)) - (b2^2) * √(1 / (b1^2 + b2^2)))^2) * f3 * (st^2) + ((-b2 * ct * √(1 / (b1^2 + b2^2)) - b1 * b3 * √(1 / (b1^2 + b2^2)) * st)^2) * f1 + ((b1 * ct * √(1 / (b1^2 + b2^2)) - b2 * b3 * √(1 / (b1^2 + b2^2)) * st)^2) * f2
D3(::Val{2}, ::Val{3}, f1, f2, f3, b1, b2, b3, ct, st) =
    -((-(b1^2) * √(1 / (b1^2 + b2^2)) - (b2^2) * √(1 / (b1^2 + b2^2)))^2) * f3 * ct * st + (-b2 * √(1 / (b1^2 + b2^2)) * st + b1 * b3 * ct * √(1 / (b1^2 + b2^2))) * (-b2 * ct * √(1 / (b1^2 + b2^2)) - b1 * b3 * √(1 / (b1^2 + b2^2)) * st) * f1 + (b1 * √(1 / (b1^2 + b2^2)) * st + b2 * b3 * ct * √(1 / (b1^2 + b2^2))) * (b1 * ct * √(1 / (b1^2 + b2^2)) - b2 * b3 * √(1 / (b1^2 + b2^2)) * st) * f2
D3(::Val{3}, ::Val{3}, f1, f2, f3, b1, b2, b3, ct, st) =
    ((-(b1^2) * √(1 / (b1^2 + b2^2)) - (b2^2) * √(1 / (b1^2 + b2^2)))^2) * f3 * (ct^2) + ((-b2 * √(1 / (b1^2 + b2^2)) * st + b1 * b3 * ct * √(1 / (b1^2 + b2^2)))^2) * f1 + ((b1 * √(1 / (b1^2 + b2^2)) * st + b2 * b3 * ct * √(1 / (b1^2 + b2^2)))^2) * f2

# D-matrix elements of 4x4 problem
D4(::Val{1}, ::Val{1}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    (b1^2) * f1 + (b2^2) * f2 + (b3^2) * f3 + (b4^2) * f4
D4(::Val{1}, ::Val{2}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    b1 * (-b4 * sy - b2 * cz * cy + b3 * cy * sz) * f1 + (b3 * sy + b1 * cz * cy + b4 * cy * sz) * b2 * f2 + (-b2 * sy - b1 * cy * sz + b4 * cz * cy) * b3 * f3 + (b1 * sy - b2 * cy * sz - b3 * cz * cy) * b4 * f4
D4(::Val{1}, ::Val{3}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    b1 * (b4 * cy * sx - b2 * (sz * cx + cz * sy * sx) - b3 * (cz * cx - sz * sy * sx)) * f1 + (-b3 * cy * sx + b1 * (sz * cx + cz * sy * sx) - b4 * (cz * cx - sz * sy * sx)) * b2 * f2 + (b2 * cy * sx + b1 * (cz * cx - sz * sy * sx) + b4 * (sz * cx + cz * sy * sx)) * b3 * f3 + (-b1 * cy * sx + b2 * (cz * cx - sz * sy * sx) - b3 * (sz * cx + cz * sy * sx)) * b4 * f4
D4(::Val{1}, ::Val{4}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    b1 * (-b4 * cy * cx - b2 * (sz * sx - cz * sy * cx) - b3 * (cz * sx + sz * sy * cx)) * f1 + (b3 * cy * cx + b1 * (sz * sx - cz * sy * cx) - b4 * (cz * sx + sz * sy * cx)) * b2 * f2 + (-b2 * cy * cx + b1 * (cz * sx + sz * sy * cx) + b4 * (sz * sx - cz * sy * cx)) * b3 * f3 + (b1 * cy * cx + b2 * (cz * sx + sz * sy * cx) - b3 * (sz * sx - cz * sy * cx)) * b4 * f4
D4(::Val{2}, ::Val{2}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    ((b3 * sy + b1 * cz * cy + b4 * cy * sz)^2) * f2 + ((-b2 * sy - b1 * cy * sz + b4 * cz * cy)^2) * f3 + ((-b4 * sy - b2 * cz * cy + b3 * cy * sz)^2) * f1 + ((b1 * sy - b2 * cy * sz - b3 * cz * cy)^2) * f4
D4(::Val{2}, ::Val{3}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    (b3 * sy + b1 * cz * cy + b4 * cy * sz) * (-b3 * cy * sx + b1 * (sz * cx + cz * sy * sx) - b4 * (cz * cx - sz * sy * sx)) * f2 + (-b2 * sy - b1 * cy * sz + b4 * cz * cy) * (b2 * cy * sx + b1 * (cz * cx - sz * sy * sx) + b4 * (sz * cx + cz * sy * sx)) * f3 + (-b4 * sy - b2 * cz * cy + b3 * cy * sz) * (b4 * cy * sx - b2 * (sz * cx + cz * sy * sx) - b3 * (cz * cx - sz * sy * sx)) * f1 + (b1 * sy - b2 * cy * sz - b3 * cz * cy) * (-b1 * cy * sx + b2 * (cz * cx - sz * sy * sx) - b3 * (sz * cx + cz * sy * sx)) * f4
D4(::Val{2}, ::Val{4}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    (b3 * sy + b1 * cz * cy + b4 * cy * sz) * (b3 * cy * cx + b1 * (sz * sx - cz * sy * cx) - b4 * (cz * sx + sz * sy * cx)) * f2 + (-b2 * sy - b1 * cy * sz + b4 * cz * cy) * (-b2 * cy * cx + b1 * (cz * sx + sz * sy * cx) + b4 * (sz * sx - cz * sy * cx)) * f3 + (-b4 * sy - b2 * cz * cy + b3 * cy * sz) * (-b4 * cy * cx - b2 * (sz * sx - cz * sy * cx) - b3 * (cz * sx + sz * sy * cx)) * f1 + (b1 * sy - b2 * cy * sz - b3 * cz * cy) * (b1 * cy * cx + b2 * (cz * sx + sz * sy * cx) - b3 * (sz * sx - cz * sy * cx)) * f4
D4(::Val{3}, ::Val{3}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    ((-b3 * cy * sx + b1 * (sz * cx + cz * sy * sx) - b4 * (cz * cx - sz * sy * sx))^2) * f2 + ((b2 * cy * sx + b1 * (cz * cx - sz * sy * sx) + b4 * (sz * cx + cz * sy * sx))^2) * f3 + ((b4 * cy * sx - b2 * (sz * cx + cz * sy * sx) - b3 * (cz * cx - sz * sy * sx))^2) * f1 + ((-b1 * cy * sx + b2 * (cz * cx - sz * sy * sx) - b3 * (sz * cx + cz * sy * sx))^2) * f4
D4(::Val{3}, ::Val{4}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    (b3 * cy * cx + b1 * (sz * sx - cz * sy * cx) - b4 * (cz * sx + sz * sy * cx)) * (-b3 * cy * sx + b1 * (sz * cx + cz * sy * sx) - b4 * (cz * cx - sz * sy * sx)) * f2 + (-b2 * cy * cx + b1 * (cz * sx + sz * sy * cx) + b4 * (sz * sx - cz * sy * cx)) * (b2 * cy * sx + b1 * (cz * cx - sz * sy * sx) + b4 * (sz * cx + cz * sy * sx)) * f3 + (-b4 * cy * cx - b2 * (sz * sx - cz * sy * cx) - b3 * (cz * sx + sz * sy * cx)) * (b4 * cy * sx - b2 * (sz * cx + cz * sy * sx) - b3 * (cz * cx - sz * sy * sx)) * f1 + (b1 * cy * cx + b2 * (cz * sx + sz * sy * cx) - b3 * (sz * sx - cz * sy * cx)) * (-b1 * cy * sx + b2 * (cz * cx - sz * sy * sx) - b3 * (sz * cx + cz * sy * sx)) * f4
D4(::Val{4}, ::Val{4}, f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) =
    ((b3 * cy * cx + b1 * (sz * sx - cz * sy * cx) - b4 * (cz * sx + sz * sy * cx))^2) * f2 + ((-b2 * cy * cx + b1 * (cz * sx + sz * sy * cx) + b4 * (sz * sx - cz * sy * cx))^2) * f3 + ((-b4 * cy * cx - b2 * (sz * sx - cz * sy * cx) - b3 * (cz * sx + sz * sy * cx))^2) * f1 + ((b1 * cy * cx + b2 * (cz * sx + sz * sy * cx) - b3 * (sz * sx - cz * sy * cx))^2) * f4

function get_D_elements(f1, f2, f3, b1, b2, b3, ct, st)
    D =                                                            #  ↓  ↓ index in D matrix
        D3(Val(1), Val(1), f1, f2, f3, b1, b2, b3, ct, st) |> abs, # (1, 1) 
        D3(Val(1), Val(2), f1, f2, f3, b1, b2, b3, ct, st) |> abs, # (1, 2) 
        D3(Val(1), Val(3), f1, f2, f3, b1, b2, b3, ct, st) |> abs, # (1, 3) 
        D3(Val(2), Val(2), f1, f2, f3, b1, b2, b3, ct, st) |> abs, # (2, 2) 
        D3(Val(2), Val(3), f1, f2, f3, b1, b2, b3, ct, st) |> abs, # (2, 3) 
        D3(Val(3), Val(3), f1, f2, f3, b1, b2, b3, ct, st) |> abs  # (3, 3)
    D
end

get_D_elements(bands::SVector{3,Band{Float64}}, theta::MVector{1,Float64}) = get_D_elements(unpack(bands)..., unpack(theta)...)

function get_D_elements(f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz)
    D =                                                                                    #  ↓  ↓ index in D matrix
        D4(Val(1), Val(1), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (1, 1)
        D4(Val(1), Val(2), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (1, 2)
        D4(Val(1), Val(3), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (1, 3)
        D4(Val(1), Val(4), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (1, 4)
        D4(Val(2), Val(2), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (2, 2)
        D4(Val(2), Val(3), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (2, 3)
        D4(Val(2), Val(4), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (2, 4)
        D4(Val(3), Val(3), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (3, 3)
        D4(Val(3), Val(4), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs, # (3, 4)
        D4(Val(4), Val(4), f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz) |> abs  # (4, 4)
    D
end

get_D_elements(bands::SVector{4,Band{Float64}}, theta::MVector{3,Float64}) = get_D_elements(unpack(bands)..., unpack(theta)...)

##############################################################################
##  Optimization  ############################################################
##############################################################################

# loss function for 3x3 problem
function loss(
    f1, f2, f3, b1, b2, b3, ct, st, w12, w13, w23
)
    sqsum = 0.0

    Del = get_D_elements(f1, f2, f3, b1, b2, b3, ct, st)
    D(i, j) = Del[index_D(Val(3), i, j)]

    # ws are tuples containing a value and the indices of
    # another matrix element, if set
    ws, idx = (w12, w13, w23), ((1, 2), (1, 3), (2, 3))
    for ((i, j), (val, other)) in zip(idx, ws)
        isnan(val) && continue
        if isnothing(other)
            sqsum += (val - D(i, j))^2
        else
            k, l = other
            # if `other` is set (indices of other matrix element), we minimize
            # the deviation of `val` to the ratio of the current matrix element
            # and the `other` matrix element
            sqsum += (val - D(i, j) / D(k, l))^2
        end
    end

    sqsum
end

# loss function for 4x4 problem
function loss(
    f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz, w12, w13, w14, w23, w24, w34,
)
    sqsum = 0.0

    Del = get_D_elements(
        f1, f2, f3, f4, b1, b2, b3, b4, cx, cy, cz, sx, sy, sz
    )
    D(i, j) = Del[index_D(Val(4), i, j)]

    # ws are tuples containing a value and the indices of
    # another matrix element, if set
    ws = (w12, w13, w14, w23, w24, w34)
    idx = ((1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4))
    for ((i, j), (val, other)) in zip(idx, ws)
        isnan(val) && continue
        if isnothing(other)
            sqsum += (val - D(i, j))^2
        else
            k, l = other
            # if `other` is set (indices of other matrix element), we minimize
            # the deviation of `val` to the ratio of the current matrix element
            # and the `other` matrix element
            sqsum += (val - D(i, j) / D(k, l))^2
        end
    end

    sqsum
end

function _solve(
    f1, f2, f3, b1, b2, b3, w12, w13, w23,
    args...;
    retries=nothing,
    seed::Union{Nothing,Int64}=nothing,
    silent=false,
    kw...
)
    # see solve function below (for 4x4 case) for comments
    !isnothing(seed) && seed!(seed)
    retries_sorted = isnothing(retries) ? RETRIES : retries
    retries_converged = isnothing(retries) ? RETRIES : retries
    tries_sorted = 0
    guess = MVector{1,Float64}(undef)
    local D
    while tries_sorted < retries_sorted
        retries_sorted += 1
        current_loss = F64MAX
        tries_converged = 0
        local theta
        while (current_loss > CONV_THRES && tries_converged < retries_converged)
            tries_converged += 1
            rand!(view(guess, 1), 0:EPS:2pi) # randomize guess for each iteration
            res = optimize(
                t -> loss(f1, f2, f3, b1, b2, b3, cos(t[1]), sin(t[1]), w12, w13, w23),
                guess, args...; kw...
            )
            theta = minimizer(res)
            current_loss = loss(f1, f2, f3, b1, b2, b3, cos(theta[1]), sin(theta[1]), w12, w13, w23)
            tries_converged == retries_converged && !silent && @warn "Could not find valid solution after $retries_converged tries, verify inputs."
        end
        D = get_D_elements(f1, f2, f3, b1, b2, b3, cos(theta[1]), sin(theta[1]))
        idx(i, j) = index_D(Val(3), i, j)
        if D[idx(2, 2)] <= D[idx(3, 3)]
            return D
        end
        tries_sorted == retries_sorted && !silent && @warn "Could not find sorted solution after $retries_sorted tries, verify inputs."
    end
    D
end

function _solve(
    f1, f2, f3, f4, b1, b2, b3, b4, w12, w13, w14, w23, w24, w34,
    args...;
    retries=nothing,
    seed::Union{Nothing,Int64}=nothing,
    silent=false,
    kw...
)
    !isnothing(seed) && seed!(seed)
    retries_sorted = isnothing(retries) ? RETRIES : retries
    retries_converged = isnothing(retries) ? RETRIES : retries
    tries_sorted = 0
    guess = MVector{3,Float64}(undef)
    local D
    while tries_sorted < retries_sorted
        retries_sorted += 1
        current_loss = F64MAX
        tries_converged = 0
        local theta
        # If we run into a local minimum, the constraints set by `couplings`
        # will not be fulfilled and the loss function will yield something
        # considerably larger than 0. In this case, we just retry a couple
        # of times with different starting guesses.
        while (current_loss > CONV_THRES && tries_converged < retries_converged)
            tries_converged += 1
            rand!(view(guess, 1:3), 0:EPS:2pi) # randomize guess for each iteration
            res = optimize(
                t -> loss(f1, f2, f3, f4, b1, b2, b3, b4, cos(t[1]), cos(t[2]), cos(t[3]), sin(t[1]), sin(t[2]), sin(t[3]), w12, w13, w14, w23, w24, w34),
                guess, args...; kw...
            )
            theta = minimizer(res)
            current_loss = loss(f1, f2, f3, f4, b1, b2, b3, b4, cos(theta[1]), cos(theta[2]), cos(theta[3]), sin(theta[1]), sin(theta[2]), sin(theta[3]), w12, w13, w14, w23, w24, w34)
            tries_converged == retries_converged && !silent && @warn "Could not find valid solution after $retries_converged tries, verify inputs."
        end
        D = get_D_elements(f1, f2, f3, f4, b1, b2, b3, b4, cos(theta[1]), cos(theta[2]), cos(theta[3]), sin(theta[1]), sin(theta[2]), sin(theta[3]))
        idx(i, j) = index_D(Val(4), i, j)
        # For a solution to be valid, we require the diagonal elements to be sorted.
        # This is to make sure that the coupling constraints that we put into the solver
        # target the correct states.
        if D[idx(2, 2)] <= D[idx(3, 3)] <= D[idx(4, 4)]
            return D
        end
        tries_converged == retries_converged && !silent && @warn "Could not find valid solution after $retries_converged tries, verify inputs."
    end
    D
end

##############################################################################
##  Helper functions #########################################################
##############################################################################

function seed!(num)
    # wrapped seed function to keep random number generator
    # stable across julia releases (hopefully)
    # https://docs.julialang.org/en/v1/stdlib/Random/#Random-Numbers
    Random.seed!(TaskLocalRNG(), num)
end

±(μ, σ) = Particles(NUM_PARTICLES, Normal(μ, σ))

# remove values < 0.0 (for intensities)
clip(x::Float64) = x < 0.0 ? 0.0 : x

function clip(x::Particles{Float64,N}) where {N}
    for n in 1:N
        if x.particles[n] < 0.0
            x.particles[n] = 0.0
        end
    end
    x
end

num_particles(::Particles{T,N}) where {T,N} = N

function index_D(::Val{3}, i, j)
    (i, j) == (1, 1) && return 1
    (i, j) == (1, 2) && return 2
    (j, i) == (1, 2) && return 2
    (i, j) == (1, 3) && return 3
    (j, i) == (1, 3) && return 3
    (i, j) == (2, 2) && return 4
    (i, j) == (2, 3) && return 5
    (j, i) == (2, 3) && return 5
    (i, j) == (3, 3) && return 6
    return 1 # unreachable
end

function index_D(::Val{4}, i, j)
    (i, j) == (1, 1) && return 1
    (i, j) == (1, 2) && return 2
    (i, j) == (1, 3) && return 3
    (i, j) == (1, 4) && return 4
    (j, i) == (1, 2) && return 2
    (j, i) == (1, 3) && return 3
    (j, i) == (1, 4) && return 4
    (i, j) == (2, 2) && return 5
    (i, j) == (2, 3) && return 6
    (i, j) == (2, 4) && return 7
    (j, i) == (2, 3) && return 6
    (j, i) == (2, 4) && return 7
    (i, j) == (3, 3) && return 8
    (i, j) == (3, 4) && return 9
    (j, i) == (3, 4) && return 9
    (i, j) == (4, 4) && return 10
    return 1 # unreachable
end

# sort D matrix elements for 3x3 problem
function sort_diag(::Val{3}, els)

    i1, wn1 = (1, els[1])
    i2, wn2 = (2, els[4])
    i3, wn3 = (3, els[6])

    # sort by wavenumber
    while !(wn1 <= wn2 <= wn3)
        if (wn1 > wn2)
            (i1, wn1), (i2, wn2) = (i2, wn2), (i1, wn1)
        end
        if (wn2 > wn3)
            (i3, wn3), (i2, wn2) = (i2, wn2), (i3, wn3)
        end
    end

    D(i, j) = els[index_D(Val(3), i, j)]
    wn1, D(i1, i2), D(i1, i3), wn2, D(i2, i3), wn3
end

function sort_diag(::Val{4}, els)

    i1, wn1 = (1, els[1])
    i2, wn2 = (2, els[5])
    i3, wn3 = (3, els[8])
    i4, wn4 = (4, els[10])

    # sort by wavenumber
    while !(wn1 <= wn2 <= wn3 <= wn4)
        if (wn1 > wn2)
            (i1, wn1), (i2, wn2) = (i2, wn2), (i1, wn1)
        end
        if (wn2 > wn3)
            (i3, wn3), (i2, wn2) = (i2, wn2), (i3, wn3)
        end
        if (wn3 > wn4)
            (i3, wn3), (i4, wn4) = (i4, wn4), (i3, wn3)
        end
    end

    D(i, j) = els[index_D(Val(4), i, j)]
    wn1, D(i1, i2), D(i1, i3), D(i1, i4), wn2, D(i2, i3), D(i2, i4), wn3, D(i3, i4), wn4
end


function preprocess_for_clustering(D::Matrix{Particles{Float64,N}}) where {N}
    m, n = size(D)
    narr = zeros(m, n, N)
    for p in 1:N, i in 1:m, j in 1:n
        narr[i, j, p] = D[i, j].particles[p]
    end
    reshape(narr, :, N)
end


##############################################################################
##  Type unpacking for low level function calls  #############################
##############################################################################

function unpack(bands::SVector{3,Band{T}}) where {T<:FermiFloat}
    f1 = bands[1].nu
    f2 = bands[2].nu
    f3 = bands[3].nu
    i1 = bands[1].int |> clip
    i2 = bands[2].int |> clip
    i3 = bands[3].int |> clip
    isum = i1 + i2 + i3
    # eigenvector components
    b1 = sqrt(i1 / isum)
    b2 = sqrt(i2 / isum)
    b3 = sqrt(i3 / isum)
    f1, f2, f3, b1, b2, b3
end

function unpack(bands::SVector{4,Band{T}}) where {T<:FermiFloat}
    f1 = bands[1].nu
    f2 = bands[2].nu
    f3 = bands[3].nu
    f4 = bands[4].nu
    i1 = bands[1].int |> clip
    i2 = bands[2].int |> clip
    i3 = bands[3].int |> clip
    i4 = bands[4].int |> clip
    isum = i1 + i2 + i3 + i4
    # eigenvector components
    b1 = sqrt(i1 / isum)
    b2 = sqrt(i2 / isum)
    b3 = sqrt(i3 / isum)
    b4 = sqrt(i4 / isum)
    f1, f2, f3, f4, b1, b2, b3, b4
end

function unpack(theta::MVector{1,Float64})
    cos(theta[1]), sin(theta[1])
end

function unpack(theta::MVector{3,Float64})
    cos(theta[1]), cos(theta[2]), cos(theta[3]), sin(theta[1]), sin(theta[2]), sin(theta[3])
end

newNaN(::Type{Float64}) = NaN
newNaN(::Type{Particles{Float64,N}}) where {N} = Particles(fill(NaN, N))

function unpack(couplings::Couplings{T,3}) where {T<:FermiFloat}
    cpls = couplings.couplings
    w12 = cpls[1, 2].is_set ? (cpls[1, 2].value, cpls[1, 2].other) : (newNaN(T), nothing)
    w13 = cpls[1, 3].is_set ? (cpls[1, 3].value, cpls[1, 3].other) : (newNaN(T), nothing)
    w23 = cpls[2, 3].is_set ? (cpls[2, 3].value, cpls[2, 3].other) : (newNaN(T), nothing)
    w12, w13, w23
end

function unpack(couplings::Couplings{T,4}) where {T<:FermiFloat}
    cpls = couplings.couplings
    w12 = cpls[1, 2].is_set ? (cpls[1, 2].value, cpls[1, 2].other) : (newNaN(T), nothing)
    w13 = cpls[1, 3].is_set ? (cpls[1, 3].value, cpls[1, 3].other) : (newNaN(T), nothing)
    w14 = cpls[1, 4].is_set ? (cpls[1, 4].value, cpls[1, 4].other) : (newNaN(T), nothing)
    w23 = cpls[2, 3].is_set ? (cpls[2, 3].value, cpls[2, 3].other) : (newNaN(T), nothing)
    w24 = cpls[2, 4].is_set ? (cpls[2, 4].value, cpls[2, 4].other) : (newNaN(T), nothing)
    w34 = cpls[3, 4].is_set ? (cpls[3, 4].value, cpls[3, 4].other) : (newNaN(T), nothing)
    w12, w13, w14, w23, w24, w34
end

end
