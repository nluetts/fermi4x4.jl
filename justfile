default:
	just -l

# render ESI for paper
esi-paper:
	podman run --rm --privileged --volume "$(pwd):/data" --user=root pandoc/extra:3.6.0 docs/esi.ipynb -f ipynb+raw_tex -o docs/esi-paper.pdf --pdf-engine=xelatex --template eisvogel --pdf-engine-opt='-interaction=nonstopmode' --listings --toc --metadata-file=docs/meta-paper.yml

# render ESI for repo
esi-repo:
	podman run --rm --privileged --volume "$(pwd):/data" --user=root pandoc/extra:3.6.0 docs/esi.ipynb -f ipynb+raw_tex -o docs/esi.pdf --pdf-engine=xelatex --template eisvogel --pdf-engine-opt='-interaction=nonstopmode' --listings --toc --metadata-file=docs/meta.yml

# build image to work on docs/esi.ipynb
build:
	podman build --tag fermi4x4-docs -f docs/Dockerfile 

# run container to work on docs/esi.ipynb
jupyter:
	podman run --rm --publish 8888:8888 --volume $(pwd):/home/jovyan/work:z --userns=keep-id fermi4x4-docs:latest
