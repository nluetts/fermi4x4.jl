# Fermi4x4.jl

## Introduction

This Julia package provides functions to solve a particular type of inverse
eigenvalue problem that comes up in vibrational spectroscopy: Given a set 3 to 4
experimental band positions and associated intensities, where it is assumed that
the excited states perturb each other due to anharmonic resonance, find a set
of _deperturbed_ wavenumbers and coupling elements that are compatible with
the experimental data.

In terms of linear algebra, the problem can be formulated as follows:

$$
\mathbf{D} = \mathbf{L} \mathbf{F} \mathbf{L}^T
$$

Here, $\mathbf{D}$ is a symmetric 3x3 or 4x4 matrix that holds deperturbed
band positions (diagonal elements) and coupling elements (off-diagonal
elements). $\mathbf{F}$ is a diagonal matrix made up of the eigenvalues of 
$\mathbf{D}$, which are the observed (perturbed) band positions. $\mathbf{L}$ is
an orthonormal matrix build from the eigenvectors of $\mathbf{D}$ and contains
information about the experimental intensities. This intensity information is,
however, not enough to fully specify $\mathbf{L}$. Thus, to solve the problem,
several assumptions are made:

- There is only one deperturbed state with a relevant ground state transition
  moment, the "bright state", while the remaining 2 to 3 deperturbed "dark
  states" have negligible transition moments.
- Some of the coupling elements are _constraint_ to certain values

With $\mathbf{F}$ being available from the experiment, the task is to find
proper $\mathbf{L}$ that yield coupling elements in the $\mathbf{D}$ matrix
that fulfill the given constraints. One row vector of $\mathbf{L}$ is derived
from experimental intensities. Finding the remaining elements is done _via_
a minimization step. Details are given in  the Jupyter notebook `docs/
derivation.ipynb` (requires Python and Numpy, and Sympy to run).

## Installation

This package is not available from Julia's general package registry and
needs to be installed explicitly _via_ the URL of its repository. In the
Julia REPL (or a Jupyter notebook), do:

```julia
import Pkg
Pkg.activate("path/to/my/project") # if you use a dedicated project,
                                   # which you should
pkg"add https://gitlab.gwdg.de/nluetts/fermi4x4.jl"
```

To install a specific version, you can append the Git commit SHA or
version tag to the command, preceded by a # symbol, like this:

```julia
pkg"add https://gitlab.gwdg.de/nluetts/fermi4x4.jl#b2ddda13812d77f4334de309b1c5636c19660952"
# or
pkg"add https://gitlab.gwdg.de/nluetts/fermi4x4.jl#0.1.0-rc.1"
```

## Usage Guide

The package provides several functions to solve the inverse eigenvalue problem.
The most common functions are:

* `solve(bands, couplings; retries=nothing, verify=true, show_verify=false, seed=nothing, kw...)`: takes a set of band positions and intensities (`bands`) and a set of coupling constraints (`couplings`) as input. It returns a matrix `D` that represents the deperturbed wavenumbers and coupling elements.
* `Band(wavenumber, intensity)`: defines a band which is used as input to the `solve` function
* `couplings_3x3()`, `couplings_4x4()`, `ucouplings_3x3()`, `ucouplings_4x4()`: create coupling constraints that you can set with the `couple!` and `uncouple!` function (see below)
* `couple!(couplings, i, j, value)`: constrains a coupling element in the `D` matrix to a specific value.
* `uncouple!(couplings, i, j)`: constrains a coupling element in the `D` matrix to zero.

To find out more about these functions, view their documentation by putting a
question mark and their name into the Julia interpreter, e.g.

```julia
julia> ?solve
```

Some helpful utility functions are `bands`, `group_uncertain`,
`select_most_common`, `select_valid` and `verify_result`. See their respective
documentation for explanations (e.g. `?group_uncertain`).

**Example Usage**

Here is an example of how to use the `solve` function to solve a 3x3 problem:

```julia
using Fermi4x4

bds = bands(
    Band(3190, 0.27),
    Band(3299, 0.57),
    Band(3348, 0.16)
)
cpls = couplings_3x3()
uncouple!(cpls, 2, 3)
D = solve(bds, cpls; seed=42)
# output:
# 3×3 Matrix{Float64}:
#  3277.41      51.3737        22.0601
#    51.3737  3221.37           9.00099e-5
#    22.0601     9.00099e-5  3338.22
```

This returns a matrix `D` that represents the deperturbed wavenumbers and
coupling elements.

**Uncertain Inputs**

The package also supports uncertain inputs. You can use the `ucouplings_3x3()`
or `ucouplings_4x4()` functions to create a set of coupling constraints for
uncertain inputs. For example:

```julia
using Fermi4x4
using Fermi4x4: ±

bds = bands(
    Band(3190 ± 1.0, 0.27 ± 0.01),
    Band(3299 ± 1.0, 0.57 ± 0.01),
    Band(3348 ± 1.0, 0.16 ± 0.01)
)
cpls = ucouplings_3x3()
uncouple!(cpls, 2, 3)
D = solve(bds, cpls; seed=42)
# output:
# 3×3 Matrix{MonteCarloMeasurements.Particles{Float64, 10000}}:
#  3277.41 ± 1.3     51.359 ± 0.775           22.0433 ± 0.74
#    51.359 ± 0.775  3221.37 ± 1.21             8.24543e-5 ± 0.000857
#    22.0433 ± 0.74     8.24543e-5 ± 0.000857  3338.22 ± 0.984
```

Note that `Fermi4x4` relies on the [`MonteCarloMeasurements`](https://baggepinnen.github.io/MonteCarloMeasurements.jl/stable/)
package to handle uncertainties. If you want to define uncertainties using the
`MonteCarloMeasurements` package directly (instead of using the `±` operator
from `Fermi4x4` like shown in the example), or you want to post-process results
from `Fermi4x4` with further uncertain inputs, note that `Fermi4x4` uses a
default number of "particles" (samples) of 10000, while `MonteCarloMeasurements`
uses 2000.


**Verification**

The package provides a `verify_result` function to verify that diagonalizing the
resulting `D` matrix leads back to the input band positions and intensities, as
it should (within some numerical accuracy). Applying this function to the uncertain
input/output of the previous example

```julia
verify_results(bds, D)
```

prints

```
5 out of 10000 draws (0.0 %) exceed the deviation thresholds (eigenvalues: 0.01, eigenvectors: 0.001).
```

You can use the keyword argument `verbose = true` to make it print which draws
are beyond the deviation thresholds and to check how large the deviations are.
For uncertain inputs, it is not unusual to have at least some outliers that
surpass the deviation thresholds. To filter out these outliers, you can use the
`select_valid` function.

If many or all samples fail the verification, you are probably enforcing
unphysical constraints that do not have a valid solution.

**Order of elements in resulting D matrix**

Note that because of the specific implementation in this package, the bright
state always corresponds to the first row/column in the resulting `D` matrix and
the dark states are ordered in ascending order of their deperturbed wavenumber
along the diagonal. Make sure to observe this when setting coupling constraints
via the `couple!` and `uncouple!` functions, which take indices of the `D`
matrix as inputs.

## License

The Fermi4x4.jl package is licensed under the MIT license.
