@testset "regression tests, example 2x2 as 3x3 problem" begin
    seed!(42)
    bds = bands(
        Band(3348.0 ± 1.0,   0.0 ± 0.0),
        Band(3299.0 ± 1.0, 0.57 ± 0.01),
        Band(3190.0 ± 1.0, 0.27 ± 0.01)
    )
    cpls = ucouplings_3x3()
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 1, 3)
    # 18 out of 10000 draws fail verification, we can ignore that here
    D = solve(bds, cpls, verify=false)
    @test D .|> pmean .|> abs ≈ [
        #! format: off
         3263.97    50.89     0.0
           50.89  3225.03     0.0
            0.0      0.0   3348.0
        #! format: on
    ] atol = 1e-2
    # test uncertainties
    @test D .|> pstd .|> abs ≈ [
        #! format: off
         1.24  0.76  0.0
         0.76  1.23  0.0
         0.0   0.0   1.0
         #! format: on
    ] atol = 1e-1
end

@testset "regression test, example 3x3 problem" begin
    bds = bands(
        Band(3348, 0.16),
        Band(3299, 0.57),
        Band(3190, 0.27),
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls; seed=42)
    @test D ≈ [
        #! format: off
         3277.41     51.374    22.06
           51.374  3221.37      0.0
           22.06      0.0    3338.22
        #! format: on
    ] atol = 1e-2
end

@testset "regression test, example 3x3 problem, with uncertainties" begin
    seed!(42)
    bds = bands(
        Band(3348.0 ± 1.0, 0.16 ± 0.01),
        Band(3299.0 ± 1.0, 0.57 ± 0.01),
        Band(3190.0 ± 1.0, 0.27 ± 0.01)
    )
    cpls = ucouplings_3x3()
    uncouple!(cpls, 2, 3)
    # 80 out of 10000 draws fail verification, we can ignore that here
    D = solve(bds, cpls; verify=false)
    @test D .|> pmean .|> abs ≈ [
        #! format: off
         3277.41     51.359    22.044
           51.359  3221.37      0.0
           22.044     0.0    3338.22
        #! format: on
    ] atol = 1e-2
    # test uncertainties
    @test D .|> pstd .|> abs ≈ [
        #! format: off
         1.32  0.78 0.74
         0.78  1.2  0.0
         0.74  0.0  0.98
        #! format: on
    ] atol = 1e-2
end

@testset "regression test, example 3x3 as 4x4 problem" begin
    seed!(121302)
    bds = bands(
        Band(3348, 0.16),
        Band(3299, 0.57),
        Band(3190, 0.27),
        Band(0, 0)
    )
    couplings = couplings_4x4()
    uncouple!(couplings, 1, 2)
    uncouple!(couplings, 2, 3)
    uncouple!(couplings, 2, 4)
    uncouple!(couplings, 3, 4)
    D = solve(bds, couplings; seed=42)
    @test D ≈ [
        #! format: off
        3277.41    0.0      51.374    22.060
           0.0     0.0       0.0       0.0
          51.374   0.0    3221.37      0.0
          22.060   0.0       0.0    3338.22
        #! format: on
    ] atol = 1e-2
end

@testset "regression test, example 3x3 as 4x4 problem, with uncertainties" begin
    seed!(9)
    bds = bands(
        Band(3348 ± 1.0, 0.16 ± 0.01),
        Band(3299 ± 1.0, 0.57 ± 0.01),
        Band(3190 ± 1.0, 0.27 ± 0.01),
        Band(0.0 ± 0.0, 0.0 ± 0.0)
    )
    couplings = ucouplings_4x4()
    uncouple!(couplings, 2, 1)
    uncouple!(couplings, 2, 3)
    uncouple!(couplings, 2, 4)
    uncouple!(couplings, 3, 4)
    D = solve(bds, couplings; retries=50)
    @test D .|> pmean .|> abs ≈ [
        #! format: off
         3277.41  0.0    51.36    22.04
            0.0   0.0     0.0      0.0
           51.36  0.0  3221.37     0.0
           22.04  0.0     0.0   3338.22
        #! format: on
    ] atol = 1e-2
    # test uncertainties
    @test D .|> pstd .|> abs ≈ [
        #! format: off
         1.32  0.0  0.77  0.74
         0.0   0.0  0.0   0.0
         0.77  0.0  1.2   0.0
         0.74  0.0  0.0   0.98
        #! format: on
    ] atol = 2e-2
end

function regression_test_example_4x4_problem(select_common=true)
    seed!(3)
    bds = bands(
        Band(3186.065 ± 1.0, 3.83203 ± 0.435),
        Band(3282.0175 ± 1.0, 7.11023 ± 0.845),
        Band(3347.1175 ± 1.0, 2.84336 ± 0.405),
        Band(3426.645 ± 1.0, 0.53742075 ± 0.14)
    )

    cpls = ucouplings_4x4()
    # keep in mind the changing order of the elements in the
    # resulting D matrix
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 2, 4)
    uncouple!(cpls, 3, 4)
    bds, D = solve(bds, cpls) |> x -> select_common ? select_most_common(bds, x) : (bds, x)
    bds, D
end

@testset "regression test, example 4x4 problem with uncertainties" begin
    _, D = regression_test_example_4x4_problem(true)
    # display(D .|> round .|> pmean)
    # display(D .|> round .|> pstd)
    @test D .|> pmean .|> abs ≈ [
        #! format: off
         3275.96    47.95    33.11    27.78
           47.95  3215.38     0.0      0.0
           33.11     0.0   3329.93     0.0
           27.78     0.0      0.0   3420.61
        #! format: on
    ] atol = 2e-2
    # test uncertainties
    @test D .|> pstd .|> abs ≈ [
        #! format: off
         3.55  1.89  1.77  2.04
         1.89  3.35  0.0   0.0
         1.77  0.0   2.42  0.0
         2.04  0.0   0.0   1.32
        #! format: on
    ] rtol = 2e-2
end
