using Distributions
using Fermi4x4
using Fermi4x4: ±, seed!
using LinearAlgebra
using MonteCarloMeasurements: pmean, pstd, Particles
using StaticArrays
using Test

include("sanity_checks.jl")
include("regression.jl")
include("edge_cases.jl")
