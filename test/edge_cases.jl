"""Separate function to play with the result in Julia REPL."""
function zick_zack_uncertain_inputs()
    # 4x4, with zick-zack coupling
    #     julia> D = [ 3250.0   30.0   40.0    0.0;
    #                    30.0 3200.0    0.0    0.0;
    #                    40.0    0.0 3300.0   20.0;
    #                     0.0    0.0   20.0 3400.0 ]
    #     julia> eigen(D)
    #     Eigen{Float64, Float64, Matrix{Float64}, Vector{Float64}}
    #     values:
    #     4-element Vector{Float64}:
    #      3183.038818942124
    #      3242.2862216923127
    #      3320.4013426849883
    #      3404.2736166805753
    #     vectors:
    #     4×4 Matrix{Float64}:
    #       0.485059   -0.700604    0.520357  0.0556844
    #      -0.857945   -0.497044    0.129656  0.00817791
    #      -0.168544    0.507891    0.818604  0.208632
    #       0.0155368  -0.0644066  -0.205683  0.976373
    seed!(94232)
    bds = bands(
        # vector elements squared
        Band(3183.0 ± 1.0, 0.235 ± 0.01),
        Band(3242.2 ± 1.0, 0.491 ± 0.01),
        Band(3320.4 ± 1.0, 0.271 ± 0.01),
        Band(3404.2 ± 1.0, 0.003 ± 0.01)
    )
    cpls = ucouplings_4x4()
    # keep in mind the changing order of the elements in the
    # resulting D matrix
    uncouple!(cpls, 1, 4)
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 2, 4)
    D = solve(bds, cpls)
    bds, cpls, D
end

@testset "zick zack results within 1σ of known input" begin
    seed!(765)
    _, _, D = zick_zack_uncertain_inputs()
    D_input = [
        #! format: off
     3250.0   30.0   40.0    0.0
       30.0 3200.0    0.0    0.0
       40.0    0.0 3300.0   20.0
        0.0    0.0   20.0 3400.0
        #! format: on
    ]
    for (out_ij, in_ij) in zip(D, D_input)
        m = pmean(out_ij)
        s = pstd(out_ij)
        if abs(m - 0.0) > 1e-4
            @test m - s <= in_ij <= m + s
        end
    end
end
