@testset "sanity checks 2x2 problem" begin
    seed!(42)
    # 2x2 problem
    bds = bands(
        Band(0, 0),
        Band(1000, 1),
        Band(1100, 1),
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 1, 2)
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls)
    # same intensity, so deperturbed wavenumbers
    # must fall exactly between perturbed wavenumbers,
    # and coupling element is half the band distance
    @test D .|> abs ≈ [
        #! format: off
     1050.0   0.0    50.0
        0.0   0.0    0.0
       50.0   0.0 1050.0
        #! format: on
    ] rtol = 1e-6
end

@testset "sanity checks 2x2 problem, from Dixon 1959" begin
    # 2x2 problem, Dixon 1959, DOI 10.1063/1.1730304 (Table I and page 260, upper left)
    bds = bands(
        Band(0, 0),
        Band(1289.3, 0.48),
        Band(1385.3, 1.0)
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 1, 2)
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls)
    # same intensity, so deperturbed wavenumbers
    # must fall exactly between perturbed wavenumbers,
    # and coupling element is half the band distance
    @test D[3, 3] - D[1, 1] |> abs ≈ 33.7297297
    @test D[1, 3] |> abs ≈ 44.9396966288

    # 2x2 problem (2), Dixon 1959

    bds = bands(
        Band(0, 0),
        Band(1951.9, 0.68),
        Band(2078.9, 1.0)
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 1, 2)
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls)
    @test D[3, 3] - D[1, 1] |> abs ≈ 24.1904761
    @test D[1, 3] |> abs ≈ 62.3374302

end

@testset "round-trip test, example 3x3 problem" begin
    bds = bands(
        Band(3190, 0.27),
        Band(3299, 0.57),
        Band(3348, 0.16)
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls; seed=42)
    eigenvalues, eigenvectors = eigen(D)
    @test eigenvalues ≈ [3190.0, 3299.0, 3348.0] atol = 0.001
    # test first row of eigenvector matrix;
    # for this, we first have to calculate the normalized eigenvector
    # components from the supplied intensities
    ints = [0.27, 0.57, 0.16]
    norm = sqrt(sum(ints))
    bs = sqrt.(ints) / norm
    # due to sorting of output matrix, this index has to be set manually
    @test abs.(eigenvectors[1, :]) ≈ bs atol = 1e-6
end

@testset "round-trip test, example 4x4 problem" begin
    bds = bands(
        Band(3186.065, 3.83203),
        Band(3282.0175, 7.11023),
        Band(3347.1175, 2.84336),
        Band(3426.645, 0.53742075)
    )
    cpls = couplings_4x4()
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 2, 4)
    uncouple!(cpls, 3, 4)
    D = solve(bds, cpls; seed=42)
    eigenvalues, eigenvectors = eigen(D)
    @test eigenvalues ≈ [3186.065, 3282.0175, 3347.1175, 3426.645] atol = 0.001
    # test first row of eigenvector matrix;
    # for this, we first have to calculate the normalized eigenvector
    # components from the supplied intensities
    ints = [3.83203, 7.11023, 2.84336, 0.53742075]
    norm = sqrt(sum(ints))
    bs = sqrt.(ints) / norm
    # due to sorting of output matrix, this index has to be set manually
    @test abs.(eigenvectors[1, :]) ≈ bs atol = 1e-6
end


# Problems with known outcome, i.e. a D matrix was the input,
# diagonalized and then recovered

@testset "known 2x2 as 3x3 problem" begin
    # 2x2 problem where wavenumbers are 3200 and 3210 cm⁻¹
    # and the coupling element is 30 cm⁻¹:
    #     julia> D = [3200.0   30.0 0.0;
    #                   30.0 3210.0 0.0;
    #                    0.0    0.0 0.0]
    # get eigenvalues and eigenvectors with
    #     julia> using LinearAlgebra
    #     julia> eigen(D)
    #     values:
    #     3-element Vector{Float64}:
    #         0.0
    #      3174.586187348509
    #      3235.4138126514913
    #     vectors:
    #     3×3 Matrix{Float64}:
    #      0.0  -0.76302   0.646375
    #      0.0   0.646375  0.76302
    #      1.0   0.0       0.0
    bds = bands(
        Band(0, 0),
        Band(3174.586187348509, 0.7630199824727257^2),
        Band(3235.4138126514913, 0.6463748961301956^2),
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 1, 2)
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls; seed=43)
    @test D .|> abs ≈ [
        #! format: off
     3200.0   0.0    30.0
        0.0   0.0     0.0
       30.0   0.0  3210.0
        #! format: on
    ] atol = 1e-2
end

@testset "known 3x3 problem" begin
    # 3x3 analogous:
    #     julia> D = [3200.0   30.0   50.0;
    #                   30.0 3210.0    0.0;
    #                   50.0    0.0 3300.0]
    #     julia> eigen(D)
    #     values:
    #     3-element Vector{Float64}:
    #      3162.741824702906,
    #      3225.3106477026213
    #      3321.9475275944733
    #     vectors:
    #     3×3 Matrix{Float64}:
    #      -0.806955  -0.434886  0.399622
    #       0.512264  -0.852125  0.107092
    #       0.293955   0.29113   0.910403
    bds = bands(
        Band(3162.741824702906,0.806954935283934^2),
        Band(3225.310647702621,0.434886259369608^2),
        Band(3321.947527594473,0.399621913603863^2),
    )
    cpls = couplings_3x3()
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls; seed=101)
    @test D .|> abs ≈ [
        #! format: off
        3200.0   30.0   50.0
          30.0 3210.0    0.0
          50.0    0.0 3300.0
        #! format: on
    ] atol = 1e-2
end

@testset "known 3x3 problem, changed band order" begin
    # again, first wavenumber (3250) between second and last wavenumber
    #     julia> D = [3250.0   30.0   50.0;
    #                   30.0 3210.0    0.0;
    #                   50.0    0.0 3300.0]
    #     julia> eigen(D)
    #     values:
    #     3-element Vector{Float64}:
    #      3187.593124617778
    #      3239.384616135462
    #      3333.0222592467603
    #     vectors:
    #     3×3 Matrix{Float64}:
    #       0.578272  -0.606034  0.546191
    #      -0.774234  -0.618726  0.133193
    #      -0.257223   0.499901  0.827004
    bds = bands(
        Band(3187.593124617778,0.578272^2),
        Band(3239.384616135462,0.606034^2),
        Band(3333.022259246760,0.546191^2),
    )
    cpls = couplings_3x3()
    # here we have to take into account that the order in the solution will be different
    # from the input matrix, 3210 will be at position (1, 1), so the coupling of 1 to 3
    # has to be set to zero
    uncouple!(cpls, 2, 3)
    D = solve(bds, cpls; seed=101)
    @test D .|> abs ≈ [
        #! format: off
        3250.0    30.0    50.0
          30.0  3210.0     0.0
          50.0     0.0  3300.0
        #! format: on
    ] atol = 1e-1
end

@testset "known 3x3 problem, changed band order, different states uncoupled" begin
    # again, but with different coupling set to 0
    # julia> D = [3250.0    0.0   50.0;
    #                0.0 3210.0    5.0;
    #               50.0    5.0 3300.0]
    # 
    # julia> eigen(D)
    # Eigen{Float64, Float64, Matrix{Float64}, Vector{Float64}}
    # values:
    # 3-element Vector{Float64}:
    #  3209.15638941897
    #  3219.792392628474
    #  3331.0512179525563
    # vectors:
    # 3×3 Matrix{Float64}:
    #  -0.199571   0.827561  0.524705
    #  -0.966227  -0.255286  0.0351322
    #   0.163024  -0.499972  0.850559
    bds = bands(
        Band(3209.15638941897,0.199571^2),
        Band(3219.79239262847,0.827561^2),
        Band(3331.05121795256,0.524705^2),
    )
    cpls = couplings_3x3()
    # here we have to take into account that the order in the solution will be different
    # from the input matrix, 3210 will be at position (1, 1), so the coupling of 1 to 3
    # has to be set to zero
    uncouple!(cpls, 1, 2)
    D = solve(bds, cpls; seed=102)
    @test D .|> abs ≈ [
        #! format: off
        3250.0    0.0   50.0
           0.0 3210.0    5.0
          50.0    5.0 3300.0
        #! format: on
    ] atol = 1e-2
end

@testset "known 4x4 problem" begin
    # 4x4 analogous:
    #     julia> D = [ 3450.0   30.0   50.0   10.0;
    #                    30.0 3210.0    0.0    0.0;
    #                    50.0    0.0 3300.0    0.0;
    #                    10.0    0.0    0.0 3280.0 ]
    #     julia> eigen(D)
    #      Eigen{Float64, Float64, Matrix{Float64}, Vector{Float64}}
    #      values:
    #      4-element Vector{Float64}:
    #       3205.838402185166
    #       3278.532532955023
    #       3286.813053208247
    #       3468.816011651565
    #      vectors:
    #      4×4 Matrix{Float64}:
    #       -0.137017   -0.137292    0.237803   -0.951749
    #        0.98772    -0.0600992   0.0928761  -0.11032
    #        0.072756    0.319767   -0.901662   -0.28189
    #        0.0184754   0.935568    0.349041   -0.0504062
    bds = bands(
        Band(3205.838402185166,0.137017^2),
        Band(3278.532532955023,0.137292^2),
        Band(3286.813053208247,0.237803^2),
        Band(3468.816011651565,0.951749^2),
    )
    cpls = couplings_4x4()
    # keep in mind the changing order of the elements in the
    # resulting D matrix
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 2, 4)
    uncouple!(cpls, 3, 4)
    D = solve(bds, cpls; seed=2310982)
    @test D .|> abs ≈ [
        #! format: off
         3450.0   30.0   10.0   50.0
           30.0 3210.0    0.0    0.0
           10.0    0.0 3280.0    0.0
           50.0    0.0    0.0 3300.0 
        #! format: on
    ] atol = 1e-2
end

@testset "known 4x4 problem, zick-zack coupling" begin
    # 4x4, with zick-zack coupling
    #     julia> D = [ 3250.0   30.0   40.0    0.0;
    #                    30.0 3200.0    0.0    0.0;
    #                    40.0    0.0 3300.0   20.0;
    #                     0.0    0.0   20.0 3400.0 ]
    #     julia> eigen(D)
    #     Eigen{Float64, Float64, Matrix{Float64}, Vector{Float64}}
    #     values:
    #     4-element Vector{Float64}:
    #      3183.038818942124
    #      3242.2862216923127
    #      3320.4013426849883
    #      3404.2736166805753
    #     vectors:
    #     4×4 Matrix{Float64}:
    #       0.485059   -0.700604    0.520357  0.0556844
    #      -0.857945   -0.497044    0.129656  0.00817791
    #      -0.168544    0.507891    0.818604  0.208632
    #       0.0155368  -0.0644066  -0.205683  0.976373
    bds = bands(
        Band(3183.038818942124,0.485059^2),
        Band(3242.286221692312,0.700604^2),
        Band(3320.401342684988,0.520357^2),
        Band(3404.273616680575,0.055684^2),
    )
    cpls = couplings_4x4()
    # keep in mind the changing order of the elements in the
    # resulting D matrix
    uncouple!(cpls, 1, 4)
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 2, 4)
    D = solve(bds, cpls; seed=32494)
    @test D .|> abs ≈ [
        #! format: off
        3250.0   30.0   40.0    0.0
          30.0 3200.0    0.0    0.0
          40.0    0.0 3300.0   20.0
           0.0    0.0   20.0 3400.0
        #! format: on
    ] atol = 1e-2
end

@testset "known 4x4 problem, assume knowledge of one coupling constant" begin
    # 4x4, with zick-zack coupling
    #     julia> D = [ 3250.0   30.0   40.0    0.0;
    #                    30.0 3200.0    0.0    0.0;
    #                    40.0    0.0 3300.0   20.0;
    #                     0.0    0.0   20.0 3400.0 ]
    #     julia> eigen(D)
    #     Eigen{Float64, Float64, Matrix{Float64}, Vector{Float64}}
    #     values:
    #     4-element Vector{Float64}:
    #      3183.038818942124
    #      3242.2862216923127
    #      3320.4013426849883
    #      3404.2736166805753
    #     vectors:
    #     4×4 Matrix{Float64}:
    #       0.485059   -0.700604    0.520357  0.0556844
    #      -0.857945   -0.497044    0.129656  0.00817791
    #      -0.168544    0.507891    0.818604  0.208632
    #       0.0155368  -0.0644066  -0.205683  0.976373
    bds = bands(
        Band(3183.038818942124,0.485059^2),
        Band(3242.286221692312,0.700604^2),
        Band(3320.401342684988,0.520357^2),
        Band(3404.273616680575,0.055684^2),
    )
    cpls = couplings_4x4()
    # keep in mind the changing order of the elements in the
    # resulting D matrix
    couple!(cpls, 1, 2, 30.0)
    uncouple!(cpls, 1, 4)
    uncouple!(cpls, 2, 3)
    uncouple!(cpls, 2, 4)
    D = solve(bds, cpls; seed=32494)
    @test D .|> abs ≈ [
        #! format: off
        3250.0   30.0   40.0    0.0
          30.0 3200.0    0.0    0.0
          40.0    0.0 3300.0   20.0
           0.0    0.0   20.0 3400.0
        #! format: on
    ] atol = 1e-2
end
